﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace Awake
{
    class AwakeApplicationContext : ApplicationContext
    {
        Container _container = new Container();
        NotifyIcon _notifyIcon;
        ToolStripMenuItem _awakeButton;
        ToolStripMenuItem _sleepButton;

        public AwakeApplicationContext()
        {
            _awakeButton = new ToolStripMenuItem("Stay &Awake", null, (o, e) => { SetAwake(true); });
            _sleepButton = new ToolStripMenuItem("Can &Sleep", null, (o, e) => { SetAwake(false); });

            var contextMenuStrip = new ContextMenuStrip();
            contextMenuStrip.Items.Add(_awakeButton);
            contextMenuStrip.Items.Add(_sleepButton);
            contextMenuStrip.Items.Add(new ToolStripSeparator());
            contextMenuStrip.Items.Add("E&xit", null, (o, ex) => { ExitThread(); });

            _notifyIcon = new NotifyIcon(_container)
            {
                ContextMenuStrip = contextMenuStrip,
                Icon = SystemIcons.Warning,
                Visible = true
            };

            SetAwake(false);
        }

        void UpdateMenus(bool awake)
        {
            _awakeButton.Checked = awake;
            _sleepButton.Checked = !awake;
        }

        void SetAwake(bool awake)
        {
            ThreadExecutionState.Current.Awake = awake;
            UpdateMenus(awake);
            _notifyIcon.Text = awake ? "Awake" : "Sleep";
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_container != null)
                    _container.Dispose();
            }
            _notifyIcon = null;
            _container = null;

            base.Dispose(disposing);
        }

        protected override void ExitThreadCore()
        {
            if (_notifyIcon != null)
            {
                _notifyIcon.Visible = false;
            }

            base.ExitThreadCore();
        }
    }
}
