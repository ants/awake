﻿namespace Awake
{
    class ThreadExecutionState
    {
        public static ThreadExecutionState Current => new ThreadExecutionState();

        private ThreadExecutionState()
        {
        }

        const NativeMethods.ExecutionState NormalFlags = NativeMethods.ExecutionState.None;
        const NativeMethods.ExecutionState AwakeFlags  = NativeMethods.ExecutionState.DisplayRequired |
                                                         NativeMethods.ExecutionState.SystemRequired;

        public bool Awake
        {
            get { return (GetState() & AwakeFlags) == AwakeFlags; }
            set { SetState(value ? AwakeFlags : NormalFlags); }
        }

        NativeMethods.ExecutionState GetState()
        {
            return NativeMethods.SetThreadExecutionState(AwakeFlags);
        }

        void SetState(NativeMethods.ExecutionState flags)
        {
            NativeMethods.SetThreadExecutionState(flags | NativeMethods.ExecutionState.Continuous);
        }
    }
}
