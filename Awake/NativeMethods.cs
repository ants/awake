﻿using System;
using System.Runtime.InteropServices;

namespace Awake
{
    static class NativeMethods
    {
        [Flags]
        public enum ExecutionState : uint
        {
            None            = 0x00000000,
            SystemRequired  = 0x00000001,
            DisplayRequired = 0x00000002,
            Continuous      = 0x80000000,
        }

        [DllImport("kernel32.dll")]
        public static extern ExecutionState SetThreadExecutionState(ExecutionState flags);
    }
}
